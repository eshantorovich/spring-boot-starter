package ru.alfaleasing.config.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.List;

public class IsDevProfilePresentCondition implements Condition {

    private static final String DEV_PROFILE = "dev";

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        String[] activeProfiles = context.getEnvironment().getActiveProfiles();
        return List.of(activeProfiles).contains(DEV_PROFILE);
    }
}
