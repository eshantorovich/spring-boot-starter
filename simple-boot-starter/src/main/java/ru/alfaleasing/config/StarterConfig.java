package ru.alfaleasing.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.alfaleasing.config.condition.ConditionalOnDevProfile;
import ru.alfaleasing.worker.ScheduledLogger;

@Slf4j
@Configuration
@ConditionalOnProperty(name = "app.log-time-cron")
@ConditionalOnDevProfile
@EnableScheduling
public class StarterConfig {

    @Bean
    public ScheduledLogger scheduledPrinter() {
        return new ScheduledLogger();
    }
}
