package ru.alfaleasing.worker;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

@Slf4j
public class ScheduledLogger {

    @Scheduled(cron = "${app.log-time-cron}")
    public void writeLogMessage() {
        log.info("Current time: " + LocalDateTime.now());
    }
}
